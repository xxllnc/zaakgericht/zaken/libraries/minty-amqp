## QA (tests, documentation)
pytest~=7.0
pytest-cov~=4.0
pytest-mock~=3.3
pytest-spec~=3.0

## Code style tools
black~=22.3
flake8~=4.0
isort~=5.10
mypy~=0.931

## QA (tests, documentation, security, licenses)
pip-audit
liccheck~=0.1
