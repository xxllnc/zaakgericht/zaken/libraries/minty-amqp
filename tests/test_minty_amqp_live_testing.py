# SPDX-FileCopyrightText: 2020 Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest


# TODO: Not Implemented yet  - will do after we're happy with the code.
@pytest.mark.live
class TestLiveRmq:
    def setup_method(self):
        """Create all needed infrastructure to do live test."""
        # Wait for RabbitMQ docker container to start
        # config = {"amqp": {"url": "http://rabbitmq:5672"}}
        pass

    def teardown_method(self):
        pass

    def test_message_publishing(self):
        """Publish Message and see if message is as expected."""
        pass
